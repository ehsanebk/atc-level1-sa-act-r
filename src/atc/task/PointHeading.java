package atc.task;

public class PointHeading extends Point {
	private double heading;

	public PointHeading(double x, double y, double heading) {
		super(x, y);
		this.heading = heading;
	}

	public double getHeading() {
		return heading;
	}
}
