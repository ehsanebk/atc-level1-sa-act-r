package atc.task;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.Border;

import actr.task.TaskComponent;

/**
 * The class that defines a Altitude label for a task interface.
 * 
 * @author EBK
 */
public class Distance extends JLabel implements TaskComponent
{
	Flight flight;
	/**
	 * Creates a new label.
	 * @param text the label text
	 * @param x the x coordinate
	 * @param y the y coordinate
	 * @param width the width
	 * @param height the height
	 */
	public Distance (Flight f, int x, int y, int width, int height) 
	{
		super (" ");
		flight = f;
		setBounds (x, y, width, height);
		setHorizontalAlignment(SwingConstants.LEFT);
		setVerticalAlignment(SwingConstants.TOP);
		setFont(new Font("Courier", Font.PLAIN, 9));
		setForeground(Color.green);
		//setBorder(BorderFactory.createLineBorder(Color.white));
		
	}


	/**
	 * Gets the kind of component (i.e., the "kind" slot of the ACT-R visual object).
	 * @return the kind string
	 */
	public String getKind () { return "distance"; }

	/**
	 * Gets the value of component (i.e., the "value" slot of the ACT-R visual object).
	 * @return the value string
	 */
	public String getValue () { return "" + flight.getDistanceToIntesection(); }
	
	
	
	
}

